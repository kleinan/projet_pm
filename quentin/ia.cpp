#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <time.h>
#include <vector>
#include "fonctions_niveau.hpp"
#define TAILLE_FENETRE 800
#define TAILLE_NIVEAU 15
#define VITESSE 1

void deplaceFantome(int *x,int *y,int tailleNiveau,RenderWindow &fenetre,char direction,int niveau[][20]);

int main()
{
    int niveau[20][20] {{1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {1,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},
                        {1,0,1,0,1,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0},
                        {1,0,0,0,1,1,1,1,1,1,0,0,0,0,1,0,0,0,0,0},
                        {1,0,1,0,1,1,0,0,0,1,0,1,1,0,1,0,0,0,0,0},
                        {1,0,1,1,1,1,1,1,1,1,0,1,1,0,1,0,0,0,0,0},
                        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
                        {1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,0,0,0,0},
                        {0,0,0,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0},
                        {0,0,0,0,1,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0},
                        {0,0,0,0,1,0,1,1,1,1,0,1,0,0,0,0,0,0,0,0},
                        {0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
                        {0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
    int x = TAILLE_FENETRE/TAILLE_NIVEAU; int y = TAILLE_FENETRE/TAILLE_NIVEAU; int i,f,k; int xm,ym; char direction = 'D';
    int tailleNiveau = TAILLE_NIVEAU;
    char directionTableau[4] = {'D','G','B','H'};
    int directionDispo = 0;
    Texture image;
    image.loadFromFile("red.png");
    Sprite fantome;
    Sprite fantome2;
    Sprite fantome3;
    Sprite fantome4;
    Sprite *fantomes[4] = {&fantome,&fantome2,&fantome3,&fantome4};
    /*std::vector<Sprite> fantomes;
    fantomes.push_back(fantome);
    fantomes.push_back(fantome2);*/
    char directionFantomes[4] = {'D','D'};
    fantome.setPosition(x,y); //faire un for pour les sprites
    fantome2.setPosition(x,y);
    fantome3.setPosition(x,y);
    fantome4.setPosition(x,y);
    fantome.setTexture(image);
    fantome2.setTexture(image);
    fantome3.setTexture(image);
    fantome4.setTexture(image);
    RenderWindow fenetre(VideoMode(TAILLE_FENETRE,TAILLE_FENETRE),"SFML");
    RectangleShape p(Vector2f(TAILLE_FENETRE/TAILLE_NIVEAU,TAILLE_FENETRE/TAILLE_NIVEAU));
    p.setPosition(x,y);
    Event event;
    srand(time(NULL));
    while(fenetre.isOpen())
    {
        while(fenetre.pollEvent(event))
        {
            switch(event.type)
            {
                case Event::Closed: fenetre.close();break;
            }
        }
        fenetre.clear();
        for(k=0;k<4;k++) //adapter k en fonction du nombre de fant�me
        {
            Vector2f positionFantome = fantomes[k]->getPosition();
            x = positionFantome.x;
            y = positionFantome.y;

            for(i=0;i<TAILLE_NIVEAU;i++) // (les deux fors) d�terminent la position du fant�me dans la matrice
            {
                if(y>=i*TAILLE_FENETRE/TAILLE_NIVEAU && y<=(i+1)*(TAILLE_FENETRE/TAILLE_NIVEAU))
                {
                    xm = i;
                }
            }
            for(f=0;f<TAILLE_NIVEAU;f++)
            {
                if(x>=f*TAILLE_FENETRE/TAILLE_NIVEAU && x<=(f+1)*(TAILLE_FENETRE/TAILLE_NIVEAU))
                {
                    ym = f;
                }
            }
            if(x==ym*TAILLE_FENETRE/TAILLE_NIVEAU && y==xm*TAILLE_FENETRE/TAILLE_NIVEAU) //pour ne pas changer de direction tout le temps
            {
                for(i=0;i<4;i++)
                {
                    directionTableau[i] = 'X';
                }
                directionDispo = 0;
                if(niveau[xm][ym+1] == 0 && directionFantomes[k] != 'G')  //d�finition des directions possibles
                {
                    directionTableau[directionDispo] = 'D';
                    directionDispo++;
                }
                if(niveau[xm-1][ym] == 0 && directionFantomes[k] != 'B')
                {
                    directionTableau[directionDispo] = 'H';
                    directionDispo++;
                }
                if(niveau[xm+1][ym] == 0 && directionFantomes[k] != 'H')
                {
                    directionTableau[directionDispo] = 'B';
                    directionDispo++;
                }
                if(niveau[xm][ym-1] == 0 && directionFantomes[k] != 'D')
                {
                    directionTableau[directionDispo] = 'G';
                    directionDispo++;
                }
                if(directionDispo == 1) //continuer dans la m�me direction
                    {directionFantomes[k] = directionTableau[directionDispo-1];
                    }
                else if(directionDispo != 0){ //affectation d'une direction al�atoire
                    directionFantomes[k] = directionTableau[rand()%(directionDispo)];}
                switch(directionFantomes[k]) //cul de sac
                {
                    case 'D':
                        if(niveau[xm][ym+1] != 1)
                            directionFantomes[k] = 'D';
                        else
                            directionFantomes[k] = 'G';
                        break;
                    case 'B':
                        if(niveau[xm+1][ym] != 1)
                            directionFantomes[k] = 'B';
                        else
                            directionFantomes[k] = 'H';
                        break;
                    case 'H':
                        if(niveau[xm-1][ym] != 1)
                            directionFantomes[k] = 'H';
                        else
                            directionFantomes[k] = 'B';
                        break;
                    case 'G':
                        if(niveau[xm][ym-1] != 1)
                            directionFantomes[k] = 'G';
                        else
                            directionFantomes[k] = 'D';
                        break;
                }
            }

            switch(directionFantomes[k]) //d�placement
            {
                case 'D':
                    x+=1;
                    break;
                case 'G':
                    x-=1;
                    break;
                case 'H':
                    y-=1;
                    break;
                case 'B':
                    y+=1;
                    break;
            }

            if((directionFantomes[k] == 'G' || directionFantomes[k] == 'D')&&(y!=xm*TAILLE_FENETRE/TAILLE_NIVEAU))
            {
                y=xm*TAILLE_FENETRE/TAILLE_NIVEAU;
            }
            else if((directionFantomes[k] == 'H' || directionFantomes[k] == 'B')&&(x!=ym*TAILLE_FENETRE/TAILLE_NIVEAU))
            {
                x=ym*TAILLE_FENETRE/TAILLE_NIVEAU;
            }
            fantomes[k]->setPosition(x,y);
        }
        dessinNiveau(niveau,fenetre,TAILLE_NIVEAU,TAILLE_FENETRE);
        for(k=0;k<4;k++) //adapter k en fonction du nombre de fant�me
        {
            fenetre.draw(*fantomes[k]);
        }
        fenetre.display();
        sleep(milliseconds(4));
    }
    return 1;

}
