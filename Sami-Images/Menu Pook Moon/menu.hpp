
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <time.h>
#include <windows.h>
#define LARGEUR_FENETRE2 1920
#define HAUTEUR_FENETRE2 1080
#define FONDMENU "Background pookmoon.png"
#define REGLES  "R�gles gris.png"
#define QUITTER "Quitter gris.png"
#define CREDITS "Cr�dits gris.png"
#define JOUER "Jouer gris.png"
#define HALO "halo-theme-fond.wav"
#define BOUTON "tone.wav"
#define SONCREDITS "moon-theme.wav"
#define SON1 "Son1.png"
#define SON2 "Son2.png"
#define FONDCRED "trounoir.png"
#define FONDREGLE "fondregles.png"
#define SONFANTOME "SonFamtome.wav"
#define SONTP "SonTp.wav"
#define SONCLE "Correct.wav"
#define SONOVER "sonGameOver.wav"
#define MORT "Mort.wav"
#define MANGE "mange.wav"
#define WIN "win.wav"
#define VOLMUSICFOND 50
#define VOLSONBOUTON 100

using namespace sf;

int menuP(int etat);
int creditP(int volume);
Font chargeFont(void);
int reglesP(int volume);


