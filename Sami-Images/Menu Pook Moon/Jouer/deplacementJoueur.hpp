#include <SFML/Graphics.hpp>
#include "../menu.hpp"
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

using namespace sf;

typedef struct
{
    int verifTop,verifBot,verifRight,verifLeft;
    int posX,posY,posMX,posMY;
    Sprite objet;

} Entite;
//MES FONCTIONS
void deplacements(Entite &J1,RenderWindow &app,int matrice[DIMENSION][DIMENSION],char &deplacement,char &nextdeplacement,int &score,int &tele,int &iTemps);
Entite positionMatricielle(Entite J1);
Entite ObstacleVerif(int matrice[DIMENSION][DIMENSION],Entite player);
Entite positionReelle(Entite J1);
int verifPositionY(Entite J1);
int verifPositionX(Entite J1);
int GameOver(Entite J1,int matrice[DIMENSION][DIMENSION],Sprite fantomes[10],int iTemps,int nbFantome);
int YouWin(Entite J1,int matrice[DIMENSION][DIMENSION]);
void animationGameover(int score, int volume);
void animationYouWin(int score, int volume,int niveau);
void teleporteur(Entite &J1,int matrice[DIMENSION][DIMENSION],RenderWindow &app);
int Jeu(int volume,int niveau,int &score);


//AUTRES FONCTIONS
char touche(RenderWindow &app);
void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille);
void dessinC(int x, int y,Color color,RenderWindow &fenetre,int taille);
void dessinNiveau(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille,Sprite pic,Sprite asteroide,Sprite trouNoir,Sprite obstacleCassable,Sprite obstacleCasse,Sprite Cle,Sprite itemFantome);
void dessinFantomes(int &x,int &y,int &xm,int &ym,Sprite fantomes[10],int &directionDispo,char directionTableau[4],char directionFantomes[4],RenderWindow &app,int niveau[DIMENSION][DIMENSION], int nbFantome);
void declarationFantome(Sprite fantomes[10],Texture textureFantome[4],int niveau[DIMENSION][DIMENSION]);

//Sound Sonne;
