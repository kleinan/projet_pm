#include <SFML/Graphics.hpp>
#include "deplacementJoueur.hpp"

#include <time.h>
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

using namespace sf;

char touche(RenderWindow &app);

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille);
SoundBuffer SonTP;
Sound SonT;
SoundBuffer SonClef;
Sound SonCle;
SoundBuffer SonFant;
Sound SonFan;
SoundBuffer SonOver;
Sound SonOve;
SoundBuffer SonMorte;
Sound SonMort;
SoundBuffer SonMange;
Sound SonMan;
SoundBuffer SonWin;
Sound SonWi;


int Jeu(int volume,int niveau,int &score)
{
    int vol;
    int i,j;
    srand(time(NULL));
    Event evenement;
    char deplacement='n',nextdeplacement;
    int tele=0; //TELE EST LE NOMBRE DE TELEPORTEURS DONT LE JOUEUR DISPOSE
    Font font;
    if(!font.loadFromFile("police/Joystix.TTF"))
        printf("pb de chargement de la police\n");
    Text textBonus;
    char bonus[100];
    sprintf(bonus,"%i",tele);
    textBonus.setString(bonus);
    textBonus.setPosition(50,0);
    textBonus.setFont(font);
    textBonus.setStyle(1);
    textBonus.setCharacterSize(25);
    textBonus.setColor(Color :: Cyan);
    Text texteScore;
    char text[20];
    sprintf(text,"Score : %i ",score);
    texteScore.setString(text);
    texteScore.setFont(font);
    texteScore.setStyle(1);
    texteScore.setCharacterSize(30);
    texteScore.setPosition(338,760);
    texteScore.setColor(Color :: Red);
  if(volume==1)
    vol = 20;
    else
     vol = 0;
        Time TempsActu;
	Time TempsTest;
	Time TempsPrec;
	Clock clock2;
	float Test = 0;
	int iTemps = 0;
	char timer[5];
	Text texteTemps;
	texteTemps.setFont(font);
    texteTemps.setCharacterSize(30);
    texteTemps.setPosition(600, 0);
    texteTemps.setColor(Color::Red);
    SoundBuffer SonJeu;
	if(!SonJeu.loadFromFile("SonJeu.wav"))
    return 1;
    Sound Sonne;
	Sonne.setBuffer(SonJeu);
	Sonne.setVolume(vol);

  SonFant.loadFromFile(SONFANTOME);
  SonFan.setBuffer(SonFant);
  SonFan.setVolume(5*vol);
  SonTP.loadFromFile(SONTP);
  SonT.setBuffer(SonTP);
  SonT.setVolume(5*vol);
  SonClef.loadFromFile(SONCLE);
  SonCle.setBuffer(SonClef);
  SonCle.setVolume(5*vol);
  SonOver.loadFromFile(SONOVER);
  SonOve.setBuffer(SonOver);
  SonOve.setVolume(3*vol);
  SonOve.setLoop(true);
  SonMorte.loadFromFile(MORT);
  SonMort.setBuffer(SonMorte);
  SonMort.setVolume(2*vol);
  SonMange.loadFromFile(MANGE);
  SonMan.setBuffer(SonMange);
  SonMan.setVolume(2*vol);
  SonWin.loadFromFile(WIN);
SonWi.setBuffer(SonWin);
SonWi.setVolume(3*vol);


    Entite J1;
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "JEU");
    Sonne.play();
    Sonne.setLoop(true);
    /*-1:Obstacle cassable, 0:Chemin , 1:Obstacle fixe, 2:Zone d'apparition des fant�mes,
     3:M�t�orite(pour le score), 4:T�l�porteur(bonus), 5:Cl� � r�cup�rer(pour obstacle), 6:Bonus fantome(invincibilit� 15 sec), 7:Obstacle d�truit*/
     int matrice[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,3,0,0,1,6,1,1,3,0,0,1,3,0,0,1,1,0,0,3,1},
        {1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1},
        {1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,0,1},
        {1,0,1,0,1,0,1,0,1,2,2,2,1,0,1,0,1,0,1,0,1},
        {1,0,0,0,1,0,1,3,1,1,2,1,1,3,1,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1},
        {0,0,0,0,1,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0},
        {1,1,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1},
        {1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1},
        {1,3,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,0,1,3,1},
        {1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1},
        {1,1,0,0,0,0,0,1,1,1,3,1,1,1,0,0,0,0,0,1,1},
        {1,1,0,1,1,1,0,0,1,1,-1,1,1,0,0,1,1,1,0,1,1},
        {1,0,0,1,3,1,1,0,0,1,-1,1,0,0,1,1,3,1,0,0,1},
        {1,0,1,1,0,1,4,1,0,0,0,0,0,1,4,1,0,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,1,6,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    int matrice2[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,3,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,3,1},
        {1,1,1,1,1,0,0,1,0,0,0,0,0,1,0,0,1,1,1,1,1},
        {1,2,0,1,0,0,1,0,0,1,-1,1,0,0,1,0,0,1,6,2,1},
        {1,1,0,1,0,1,0,0,1,1,-1,1,1,0,0,1,0,0,0,1,1},
        {1,1,0,1,0,1,0,1,1,1,3,1,1,1,0,1,1,1,0,1,1},
        {1,0,0,0,0,0,0,-1,-1,3,4,3,-1,-1,0,0,0,0,0,0,1},
        {1,0,1,1,1,1,0,1,1,1,3,1,1,1,0,1,1,1,1,0,1},
        {1,0,1,1,1,1,0,0,1,1,-1,1,1,0,0,1,0,0,0,0,1},
        {0,0,0,0,0,0,0,0,0,1,-1,1,0,0,1,0,0,1,1,0,0},
        {1,0,1,1,1,1,0,1,0,1,-1,1,0,1,0,0,1,3,1,0,1},
        {1,0,1,2,0,1,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1},
        {1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,0,1,2,1},
        {1,0,1,1,3,1,0,0,0,1,0,1,0,0,0,0,0,0,1,1,1},
        {1,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1,1,0,1,3,1},
        {1,0,0,0,1,1,1,0,1,0,1,0,1,0,0,1,0,0,1,0,1},
        {1,0,1,1,5,0,0,0,1,0,6,0,1,0,1,3,0,1,1,0,1},
        {1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1},
        {1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    app.clear();
    J1.posMX=10;
    J1.posMY=12;
    J1.posX=J1.posMX*LARGEUR_FENETRE/DIMENSION;
    J1.posY=J1.posMY*LARGEUR_FENETRE/DIMENSION;
    Texture pmR,pmL,pmU,pmD;
    Texture wall,textureAsteroide,textureTrouNoir,textureCle,textureCassable,textureCasse,textureItemFantome;
    Sprite mur,asteroide,trouNoir,obstacleCasse,obstacleCassable,cle,itemFantome;
    if (!pmR.loadFromFile("pooq moon petit.png")|| !pmL.loadFromFile("pooq moon left 35x35.png") ||
        !pmU.loadFromFile("pooq moon up 35x35.png") ||!pmD.loadFromFile("pooq moon down 35x35.png") ||
         !wall.loadFromFile("GalaxiePetit.png") || !textureAsteroide.loadFromFile("asteroide.png") ||
         !textureTrouNoir.loadFromFile("item trou noir.png") || !textureCassable.loadFromFile("GalaxiePetitCasse.png") ||
        !textureCasse.loadFromFile("galaxie trou 35x35.png") || !textureCle.loadFromFile("Cle.png") ||
        !textureItemFantome.loadFromFile("pookmoon famtome.png"))
        {
            printf("probleme chargement des images!");
            return EXIT_FAILURE;
        }
    cle.setTexture(textureCle);
    itemFantome.setTexture(textureItemFantome);
    obstacleCassable.setTexture(textureCassable);
    obstacleCasse.setTexture(textureCasse);
    asteroide.setTexture(textureAsteroide);
    trouNoir.setTexture(textureTrouNoir);
    mur.setTexture(wall);
    J1.objet.setTexture(pmR);
    J1.objet.setPosition(J1.posX,J1.posY);
    Texture image1,image2,image3,image4;
    image1.loadFromFile("famtome soleil vert 35px.png");
    image2.loadFromFile("famtome soleil orang e35px.png");
    image3.loadFromFile("famtome soleil bleu 35px.png");
    image4.loadFromFile("famtome galaxie 35px.png");

    Texture textureFantome[4];
    textureFantome[0] = image1;
    textureFantome[1] = image2;
    textureFantome[2] = image3;
    textureFantome[3] = image4;

    Sprite fantomes[10];
    char directionFantomes[10] = {'H','H','H','H','H','H','H','H','H','H'};
    int x=10,y=7;
    x=x*LARGEUR_FENETRE/DIMENSION;
    y=y*HAUTEUR_FENETRE/DIMENSION;
    int directionDispo = 0;
    int xm,ym;
    char directionTableau[10] = {'D','G','B','H'};
    int nbFantome = 0;

    app.draw(J1.objet);
    app.display();
    J1=ObstacleVerif(matrice,J1);
    J1=positionMatricielle(J1);
    switch(niveau)
    {
    case 1:
        declarationFantome(fantomes,textureFantome,matrice);
        break;
    case 2:
        declarationFantome(fantomes,textureFantome,matrice2);
        for(i=0;i<DIMENSION;i++)
            for(j=0;j<DIMENSION;j++)
                {
                    matrice[j][i]=matrice2[j][i];
                }
        break;
    }

    while((YouWin(J1,matrice))==0 && GameOver(J1,matrice,fantomes,iTemps,nbFantome)==0 && app.isOpen())
    {
        dessinNiveau(matrice,app,DIMENSION,mur,asteroide,trouNoir,obstacleCassable,obstacleCasse, cle,itemFantome);
        trouNoir.setPosition(0,0);
        app.draw(trouNoir);
        if(iTemps>0)
        {
            itemFantome.setPosition(550,0);
            app.draw(itemFantome);
            TempsActu = clock2.getElapsedTime();
            TempsTest = TempsActu - TempsPrec;
            Test = TempsTest.asSeconds();
            if (Test > 1)
            {
            iTemps--;
            TempsPrec = TempsActu;
            }
        }
        nbFantome = 4 + (score/2);
        sprintf(text,"Score : %i ",score);
        sprintf(bonus,"%i",tele);
        texteScore.setString(text);
        textBonus.setString(bonus);


        deplacements(J1,app,matrice,deplacement,nextdeplacement,score,tele,iTemps);
        dessinFantomes(x,y,xm,ym,fantomes,directionDispo,directionTableau,directionFantomes,app,matrice,nbFantome);




       if(iTemps>0)
       {
        TempsActu = clock2.getElapsedTime();
        TempsTest = TempsActu - TempsPrec;
        Test = TempsTest.asSeconds();
        if (Test > 1)
        {
            iTemps--;
            TempsPrec = TempsActu;
        }
        sprintf(timer,"%i",iTemps);
        texteTemps.setString(timer);
        app.draw(texteTemps);
       }

        app.draw(texteScore);
        app.draw(textBonus);
           app.display();
         app.clear();
         sleep(milliseconds(4));

    }

    if(GameOver(J1,matrice,fantomes,iTemps,nbFantome))
    {
        SonMort.play();
        app.close();
        Sonne.stop();
        animationGameover(score,volume);
    }
    else if(app.isOpen())
    {

        app.close();
        Sonne.stop();
        animationYouWin(score,volume,niveau);
    }


        Sonne.stop();



    return volume;
}

void dessinS(int x, int y,Sprite pic,RenderWindow &fenetre,int taille)
{

    pic.setPosition(x,y);
    fenetre.draw(pic);
}

void dessinNiveau(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille,Sprite pic,Sprite asteroide,Sprite trouNoir,Sprite obstacleCassable,Sprite obstacleCasse,Sprite Cle,Sprite itemFantome)
{
    int i;
    int f;
    for(i=0; i<taille; i++)
    {
        for(f=0; f<taille; f++)
        {
            if(niveau[f][i] == 0)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color::Black,fenetre,taille);
            else if(niveau[f][i] == 1)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),pic,fenetre,taille);
            else if(niveau[f][i] == 3)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),asteroide,fenetre,taille);
            else if(niveau[f][i] == 4)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),trouNoir,fenetre,taille);
            else if(niveau[f][i] == 2)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color(100,100,0),fenetre,taille);
            else if(niveau[f][i] == 5)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Cle,fenetre,taille);
            else if(niveau[f][i] == 6)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),itemFantome,fenetre,taille);
            else if(niveau[f][i] == -1)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),obstacleCassable,fenetre,taille);
            else if(niveau[f][i] == 7)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),obstacleCasse,fenetre,taille);

        }
    }
}
char touche(RenderWindow &app)
{
    int ouias = 1;
    char res='n';
    Event event;

    while (app.pollEvent(event))
    {
        // Close window : exit
        if (event.type == Event::Closed)
        {
            app.close();
//            Sonne.stop();
        }
        if (event.type == Event::MouseButtonPressed)
            res='s';
        if (event.type == Event::KeyPressed)
            switch(event.key.code)
            {
            case 57:
                res='s';
                break;
            case 25:
                res ='h';
                break;

            case 73:
                res ='h';
                break;

            case 16:
                res ='g';
                break;

            case 71:
                res ='g';
                break;

            case 18:
                res ='b';
                break;

            case 74:
                res ='b';
                break;

            case 3:
                res ='d';
                break;

            case 72:
                res ='d';
                break;

            default:
                res='n';
                break;
            }
    }



    return res;
}
