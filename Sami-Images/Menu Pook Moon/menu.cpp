﻿#include "Jouer/deplacementJoueur.hpp"
#include "menu.hpp"
using namespace sf;
Event evenement;
Texture son1;
Sprite so1;
Texture fond;
Sprite  fonde;
Texture jouer;
Sprite  joue;
Texture quitter;
Sprite  quitte;
Texture regles;
Sprite regle;
Texture credits;
Sprite credit;
Texture son2;
Sprite so2;



int menuP(int etat)
{
	/*Clock clock2;
	float Test = 0;
	Time TempsActu;
	Time TempsTest;
	Time TempsPrec; */
	int passage1 = 0;
	int passage2 = 0;
	int passage3 = 0;
	int passage4 = 0;
	int iTempsActu = 0;
	int iTempsPrec = 0;
	int iTemps = 60;
	int score = 0;
	int niveau = 1;
	Font font;
	font = chargeFont();
	Text texteScore;
	char text[20];


	//Fond et Texte Affich�

	fond.loadFromFile(FONDMENU);
	fonde.setTexture(fond);
	jouer.loadFromFile(JOUER);
	joue.setTexture(jouer);
	quitter.loadFromFile(QUITTER);
	quitte.setTexture(quitter);
	regles.loadFromFile(REGLES);
	regle.setTexture(regles);
	credits.loadFromFile(CREDITS);
	credit.setTexture(credits);
		son1.loadFromFile(SON1);
	so1.setTexture(son1);
	son2.loadFromFile(SON2);
	so2.setTexture(son2);
	so1.setPosition(200, HAUTEUR_FENETRE2 - 300);
	so2.setPosition(200, HAUTEUR_FENETRE2 - 300);
	joue.setPosition(LARGEUR_FENETRE2 / 2 - 200, HAUTEUR_FENETRE2 / 2 - 100);
	regle.setPosition(LARGEUR_FENETRE2 / 2 - 200, HAUTEUR_FENETRE2 / 2 + 25);
	quitte.setPosition(LARGEUR_FENETRE2 / 2 - 200, HAUTEUR_FENETRE2 / 2 + 150);
	credit.setPosition(LARGEUR_FENETRE2 - 400, HAUTEUR_FENETRE2 - 200);
	// Musique de Fond
	SoundBuffer bufferFond;
	if (!bufferFond.loadFromFile(HALO))
		return 1;
	Sound HaloMusic;
	HaloMusic.setBuffer(bufferFond);
	HaloMusic.setVolume(VOLMUSICFOND);
	HaloMusic.play();
	HaloMusic.setLoop(true);

	SoundBuffer bufferbouton;
	if (!bufferbouton.loadFromFile(BOUTON))
		return 1;
	Sound BoutonSon;
	BoutonSon.setBuffer(bufferbouton);
	BoutonSon.setVolume(VOLSONBOUTON);
	RenderWindow menu(VideoMode(LARGEUR_FENETRE2, HAUTEUR_FENETRE2), "Menu");
	while (menu.isOpen())
	{

		while (menu.pollEvent(evenement))
		{

			switch (evenement.type)
			{

			case Event::Closed:
				menu.close();
				break;
			case Event::MouseMoved:
				// Jouer s'affiche en rouge des texte quand on passe la souris dessus
				if ((evenement.mouseMove.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseMove.x <= LARGEUR_FENETRE2 / 2 + 140) && (evenement.mouseMove.y >= HAUTEUR_FENETRE2 / 2 - 100 && evenement.mouseMove.y <= HAUTEUR_FENETRE2 / 2 + 40))
				{

					joue.setColor(Color::Red);
					if (passage1 == 0)  // Ne pas rejouer le son tant qu'on est pas sortit du texte
						BoutonSon.play(); //Son quand on passe le curseur sur une touche
					passage1 = 1;
				}
				else
				{
					passage1 = 0;
					joue.setColor(Color::White);
				}
                // Regle s'affiche en rouge quand on passe la souris dessus

				if ((evenement.mouseMove.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseMove.x <= LARGEUR_FENETRE2 / 2 + 140) && (evenement.mouseMove.y >= HAUTEUR_FENETRE2 / 2 + 45 && evenement.mouseMove.y <= HAUTEUR_FENETRE2 / 2 + 165))
				{

					regle.setColor(Color::Red);
					if (passage2 == 0)      // Ne pas rejouer le son tant qu'on est pas sortit du texte
						BoutonSon.play();   //Son quand on passe le curseur sur une touche
					passage2 = 1;
				}

				else
				{
					passage2 = 0;
					regle.setColor(Color::White);
				}
								// Quitter s'affiche en rouge quand on passe la souris dessus

				if ((evenement.mouseMove.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseMove.x <= LARGEUR_FENETRE2 / 2 + 90) && (evenement.mouseMove.y >= HAUTEUR_FENETRE2 / 2 + 170 && evenement.mouseMove.y <= HAUTEUR_FENETRE2 / 2 + 290))
				{
					quitte.setColor(Color::Red);
					if (passage3 == 0)       // Ne pas rejouer le son tant qu'on est pas sortit du texte
						BoutonSon.play(); //Son quand on passe le curseur sur une touche
					passage3 = 1;
				}
				else
				{
					passage3 = 0;
					quitte.setColor(Color::White);
				}
				// Credit s'affiche en rouge quand on passe la souris dessus
				if ((evenement.mouseMove.x >= LARGEUR_FENETRE2 - 400 && evenement.mouseMove.x <= LARGEUR_FENETRE2 - 60) && (evenement.mouseMove.y >= HAUTEUR_FENETRE2 - 200 && evenement.mouseMove.y <= HAUTEUR_FENETRE2 - 60))
				{
					credit.setColor(Color::Red);
					if (passage4 == 0)   // Ne pas rejouer le son tant qu'on est pas sortit du texte
						BoutonSon.play(); //Son quand on passe le curseur sur une touche
					passage4 = 1;
				}

				else
				{
					passage4 = 0;
					credit.setColor(Color::White);
				}
				break;
			case Event::MouseButtonPressed:
				if ((evenement.mouseButton.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseButton.x <= LARGEUR_FENETRE2 / 2 + 90) && (evenement.mouseButton.y >= HAUTEUR_FENETRE2 / 2 + 170 && evenement.mouseButton.y <= HAUTEUR_FENETRE2 / 2 + 290))
				{
					HaloMusic.stop();
					menu.close();
					return 1;
				}
				// Coupe le son quand on clique sur l'icone
				if ((evenement.mouseButton.x >= 200 && evenement.mouseButton.x <= 300) && (evenement.mouseButton.y >= HAUTEUR_FENETRE2 - 300 && evenement.mouseButton.y <= HAUTEUR_FENETRE2 - 200))
				{
					etat = -etat;
				}

                // Permet d'afficher la fenetre des crédits
				if ((evenement.mouseButton.x >= LARGEUR_FENETRE2 - 400 && evenement.mouseButton.x <= LARGEUR_FENETRE2 - 60) && (evenement.mouseButton.y >= HAUTEUR_FENETRE2 - 200 && evenement.mouseButton.y <= HAUTEUR_FENETRE2 - 60))
				{
					HaloMusic.stop();
					menu.close();
					creditP(etat);

				}

				//Lance une partie quand on clique sur jouer
				if ((evenement.mouseButton.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseButton.x <= LARGEUR_FENETRE2 / 2 + 140) && (evenement.mouseButton.y >= HAUTEUR_FENETRE2 / 2 - 100 && evenement.mouseButton.y <= HAUTEUR_FENETRE2 / 2 + 40))
				{
					HaloMusic.stop();
					menu.close();
					etat = Jeu(etat,niveau,score);
				}
				if(score == 11)
                    niveau++;
				if ((evenement.mouseButton.x >= LARGEUR_FENETRE2 / 2 - 200 && evenement.mouseButton.x <= LARGEUR_FENETRE2 / 2 + 140) && (evenement.mouseButton.y >= HAUTEUR_FENETRE2 / 2 + 45 && evenement.mouseButton.y <= HAUTEUR_FENETRE2 / 2 + 165))
				{
				    HaloMusic.stop();
					menu.close();
                    reglesP(etat);
				}
				break;


			}
		}
		// Clear screen
		menu.clear();
		Time TempsTest;
		if(!menu.isOpen()){
                menu.create(VideoMode(LARGEUR_FENETRE2, HAUTEUR_FENETRE2), "Menu");
                HaloMusic.play();
		}

			                                                          //Première prise de temps

            /*TempsActu = clock2.getElapsedTime(); Timer de Test

			TempsTest = TempsActu - TempsPrec;
			Test = TempsTest.asSeconds();
			if (Test > 1) {
				iTemps--;
				TempsPrec = TempsActu;
			}
			printf("%i \n ", iTemps);
			printf(" Temps actu : %f \n", TempsActu.asSeconds());
			printf("Temps prec : %f \n ", TempsPrec.asSeconds());
				sprintf(text, "Temps Restant : %i ", iTemps);
				texteScore.setString(text);
				texteScore.setFont(font);
				texteScore.setCharacterSize(30);
				texteScore.setPosition(100, 100);
				texteScore.setColor(Color::Red);
*/
		// Draw the sprite

		menu.draw(fonde);
		// Permet d'afficher le bon etat de son : Allumé / Eteint
		if (etat == 1) {
			menu.draw(so1);
			HaloMusic.setVolume(VOLMUSICFOND);
			BoutonSon.setVolume(VOLSONBOUTON);
		}
		else
		{
			HaloMusic.setVolume(0);
			BoutonSon.setVolume(0);
			menu.draw(so2);
		}
		menu.draw(texteScore);
		menu.draw(joue);
		menu.draw(regle);
		menu.draw(quitte);
		menu.draw(credit);
		// Update the window
		menu.display();

	}

	return etat;
}
