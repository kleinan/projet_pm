
#include "menu.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
using namespace sf;



int reglesP(int volume) {
    Event evenement3;
Texture FondRegle;
Sprite FondRegl;
Text retour2;
Texture son5;
Sprite so5;
Texture son6;
Sprite so6;
 Font font;
    if(!font.loadFromFile("police/Joystix.TTF"))
        printf("pb de chargement de la police\n");	FondRegle.loadFromFile(FONDREGLE);
	FondRegl.setTexture(FondRegle);
	RenderWindow Regles(VideoMode(LARGEUR_FENETRE2, HAUTEUR_FENETRE2), "Regles");
	chargeFont();
	SoundBuffer SonRegle;               // Son quand on passe sur une touche
	if (!SonRegle.loadFromFile(SONCREDITS))
		return 1;
	Sound RegleSon;
	RegleSon.setBuffer(SonRegle);
	RegleSon.play();
	RegleSon.setLoop(true);
	SoundBuffer bufferbouton;               // Son quand on passe sur une touche
	if (!bufferbouton.loadFromFile(BOUTON))
		return 1;
	Sound BoutonSon;
	BoutonSon.setBuffer(bufferbouton);

	if (volume == 1) {
		BoutonSon.setVolume(VOLSONBOUTON);
		RegleSon.setVolume(VOLMUSICFOND);
	}
	else
	{
		BoutonSon.setVolume(0);
		RegleSon.setVolume(0);
	}


	son5.loadFromFile(SON1);
	so5.setTexture(son5);
	son6.loadFromFile(SON2);
	so6.setTexture(son6);
	so5.setPosition(200, HAUTEUR_FENETRE2 - 300);
	so6.setPosition(200, HAUTEUR_FENETRE2 - 300);

	retour2.setFont(font);
	retour2.setString("Retour");
	retour2.setCharacterSize(40);
	retour2.setPosition(LARGEUR_FENETRE2 / 2 - 265, HAUTEUR_FENETRE2 - 150);
	retour2.setColor(Color::Cyan);

	while (Regles.isOpen())
	{

		while (Regles.pollEvent(evenement3))
		{

			switch (evenement3.type)
			{

			case Event::Closed:
				Regles.close();
				break;
			case Event::MouseMoved:
				// Surlignage en rouge de retour quand on passe la souris dessus
				if ((evenement3.mouseMove.x >= LARGEUR_FENETRE2 / 2 - 265 && evenement3.mouseMove.x <= LARGEUR_FENETRE2 / 2 -105) && (evenement3.mouseMove.y >= HAUTEUR_FENETRE2 - 150 && evenement3.mouseMove.y <= HAUTEUR_FENETRE2 - 110))
				{
					retour2.setColor(Color::Red);
					BoutonSon.play();
				}
				else
					retour2.setColor(Color::White);
				break;
			case Event::MouseButtonPressed:
			    // Retour au menu quand on clique sur retour
				if ((evenement3.mouseButton.x >= LARGEUR_FENETRE2 / 2 - 265 && evenement3.mouseButton.x <= LARGEUR_FENETRE2 / 2 - 105) && (evenement3.mouseButton.y >= HAUTEUR_FENETRE2 - 150 && evenement3.mouseButton.y <= HAUTEUR_FENETRE2 - 110))
				{
					RegleSon.stop();
					Regles.close();
				}
				// Coupe le son quand on clique sur l'icone de son
				if ((evenement3.mouseButton.x >= 200 && evenement3.mouseButton.x <= 300) && (evenement3.mouseButton.y >= HAUTEUR_FENETRE2 - 300 && evenement3.mouseButton.y <= HAUTEUR_FENETRE2 - 200))
				{
					volume = -volume;
				}

			}

		}

		Regles.clear();
		Regles.draw(FondRegl);
				// Permet d'afficher le bon etat de son : Allum� / Eteint

		if (volume == 1) {
			Regles.draw(so5);
			RegleSon.setVolume(VOLMUSICFOND);
			BoutonSon.setVolume(VOLSONBOUTON);
		}
		else
		{
			RegleSon.setVolume(0);
			BoutonSon.setVolume(0);
			Regles.draw(so6);
		}

		Regles.draw(retour2);
		Regles.display();
	}
	return volume;
}

Font chargeFont(void)
{
	Font f;
	if (!f.loadFromFile("police/Joystix.TTF"))
	{
		printf("pb de chargement de police Joystix \n");
		exit(1);
	}
	return f;
}
