﻿#include "menu.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
using namespace sf;



int creditP(int volume) {
    Event evenement2;
Texture FondCredit;
Sprite FondCred;
Text nom1, nom2, nom3, nom4, titre, retour;
Texture son3;
Sprite so3;
Texture son4;
Sprite so4;
    int KOKO=0;//---------------------------------------------------------------------------------------------------------------------
   const int codeKONAMI[11]={73,73,74,74,71,72,71,72,1,0};//------------------------------------------------------------------------------------------
  int afficheEasterEgg=0;//------------------------------------------------------------------------------------------

     Font font;
    if(!font.loadFromFile("police/Joystix.TTF"))
        printf("pb de chargement de la police\n");
    FondCredit.loadFromFile(FONDCRED);
	FondCred.setTexture(FondCredit);
	RenderWindow credit(VideoMode(LARGEUR_FENETRE2, HAUTEUR_FENETRE2), "Credits");
	chargeFont();
	SoundBuffer SonCred;               // Son quand on passe sur une touche
	if (!SonCred.loadFromFile(SONCREDITS))
		return 1;
	Sound CredSon;
	CredSon.setBuffer(SonCred);
	CredSon.play();
	CredSon.setLoop(true);
	SoundBuffer bufferbouton;               // Son quand on passe sur une touche
	if (!bufferbouton.loadFromFile(BOUTON))
		return 1;
	Sound BoutonSon;
	BoutonSon.setBuffer(bufferbouton);

	if (volume == 1) {
		BoutonSon.setVolume(VOLSONBOUTON);
		CredSon.setVolume(VOLMUSICFOND);
	}
	else
	{
		BoutonSon.setVolume(0);
		CredSon.setVolume(0);
	}


	son3.loadFromFile(SON1);
	so3.setTexture(son3);
	son4.loadFromFile(SON2);
	so4.setTexture(son4);
	so3.setPosition(200, HAUTEUR_FENETRE2 - 300);
	so4.setPosition(200, HAUTEUR_FENETRE2 - 300);
	nom1.setFont(font);
	nom1.setCharacterSize(50);
	nom1.setPosition(LARGEUR_FENETRE2 / 2 - 215, HAUTEUR_FENETRE2 / 2 - 100);
	nom1.setString("BENSAID Sami");
	nom1.setColor(Color::White);
	nom2.setFont(font);
	nom2.setCharacterSize(50);
	nom2.setPosition(LARGEUR_FENETRE2 / 2 - 215, HAUTEUR_FENETRE2 / 2);
	nom2.setString("KLEIN Antoine");
	nom2.setColor(Color::White);
	nom3.setFont(font);
	nom3.setCharacterSize(50);
	nom3.setPosition(LARGEUR_FENETRE2 / 2 - 215, HAUTEUR_FENETRE2 / 2 + 100);
	nom3.setString("CHANOVE Adrien");
	nom3.setColor(Color::White);
	nom4.setFont(font);
	nom4.setCharacterSize(50);
	nom4.setPosition(LARGEUR_FENETRE2 / 2 - 215, HAUTEUR_FENETRE2 / 2 + 200);
	nom4.setString("COLLET Quentin");
	nom4.setColor(Color::White);
	titre.setFont(font);
	titre.setString("Credits");
	titre.setCharacterSize(150);
	titre.setPosition(LARGEUR_FENETRE2 / 2 - 370, 200);
	titre.setColor(Color::Blue);
	retour.setFont(font);
	retour.setString("Retour");
	retour.setCharacterSize(40);
	retour.setPosition(LARGEUR_FENETRE2 / 2 - 65, HAUTEUR_FENETRE2 - 200);
	retour.setColor(Color::Cyan);

	while (credit.isOpen())
	{

		while (credit.pollEvent(evenement2))
		{

			switch (evenement2.type)
			{

			case Event::Closed:
				credit.close();
				break;
            case Event::KeyPressed://------------------------------------------------------------------------------------------
                if(evenement2.key.code==codeKONAMI[KOKO] )//------------------------------------------------------------------------------------------
                {

                    KOKO++;//------------------------------------------------------------------------------------------
                    if(KOKO==10){//------------------------------------------------------------------------------------------
                    afficheEasterEgg =1;//-----------------------------------------------------------------------------------------
                    }

                }
                else//------------------------------------------------------------------------------------------
                    KOKO=0;//------------------------------------------------------------------------------------------
                break;//------------------------------------------------------------------------------------------
			case Event::MouseMoved:
				// Surlignage en rouge des texte quand on passe la souris dessus
				if ((evenement2.mouseMove.x >= LARGEUR_FENETRE2 / 2 - 65 && evenement2.mouseMove.x <= LARGEUR_FENETRE2 / 2 + 105) && (evenement2.mouseMove.y >= HAUTEUR_FENETRE2 - 200 && evenement2.mouseMove.y <= HAUTEUR_FENETRE2 - 160))
				{
					retour.setColor(Color::Red);
				}
				else
					retour.setColor(Color::White);
				break;
			case Event::MouseButtonPressed:
				if ((evenement2.mouseButton.x >= LARGEUR_FENETRE2 / 2 - 65 && evenement2.mouseButton.x <= LARGEUR_FENETRE2 / 2 + 105) && (evenement2.mouseButton.y >= HAUTEUR_FENETRE2 - 200 && evenement2.mouseButton.y <= HAUTEUR_FENETRE2 - 160))
				{
					CredSon.stop();
					credit.close();
				}
				if ((evenement2.mouseButton.x >= 200 && evenement2.mouseButton.x <= 300) && (evenement2.mouseButton.y >= HAUTEUR_FENETRE2 - 300 && evenement2.mouseButton.y <= HAUTEUR_FENETRE2 - 200))
				{
					volume = -volume; //COupe le son quand on appuie sur l'icone de son
				}

			}

		}

		credit.clear();
		credit.draw(FondCred);
		if (volume == 1) {
			credit.draw(so3);
			CredSon.setVolume(VOLMUSICFOND);
			BoutonSon.setVolume(VOLSONBOUTON);
		}
		else
		{
			CredSon.setVolume(0);
			BoutonSon.setVolume(0);
			credit.draw(so4);
		}
		credit.draw(nom1);
		credit.draw(nom2);
		credit.draw(nom3);
		credit.draw(nom4);
		credit.draw(titre);
		credit.draw(retour);
		credit.display();
		if(afficheEasterEgg)//-----------------------------------------------------------------------------------------------
        {
            nom3.setString("KONAMI code by C.A");//--------------------------------------------------------------------------
        }
	}
	return volume;
}


