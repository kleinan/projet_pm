#include "fonctions_niveau.hpp"

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille,int tailleFenetre)
{
    RectangleShape r(Vector2f(tailleFenetre/taille,tailleFenetre/taille));
    r.setPosition(x,y);
    r.setFillColor(Color(color.r,color.g,color.b));
    fenetre.draw(r);
}

void dessinNiveau(int niveau[][21],RenderWindow &fenetre,int taille,int tailleFenetre)
{
    int i;
    int f;
    for(i=0; i<taille; i++)
    {
        for(f=0; f<taille; f++)
        {
            if(niveau[f][i] == 0)
                dessinR(i*(tailleFenetre/taille),f*(tailleFenetre/taille),Color::Black,fenetre,taille,tailleFenetre);
            else if(niveau[f][i] == 1)
                dessinR(i*(tailleFenetre/taille),f*(tailleFenetre/taille),Color::Blue,fenetre,taille,tailleFenetre);
            else if(niveau[f][i] == 2)
                dessinR(i*(tailleFenetre/taille),f*(tailleFenetre/taille),Color::Yellow,fenetre,taille,tailleFenetre);
        }
    }
}
