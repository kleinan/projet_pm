#include <SFML/Graphics.hpp>
#include "fonctions_niveau.hpp"
#include "fonction_menu.hpp"

#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

typedef struct
{
    int verifTop,verifBot,verifRight,verifLeft;
    int posX,posY,posMX,posMY;
    Sprite objet;

} Entite;

int main()
{
    int taille=LARGEUR_FENETRE/DIMENSION;
    int i,j;
    Entite J1;
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "SFML window");
    int matrice[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,0,0,1,0,1,1,0,0,0,1,0,0,0,1,1,0,0,0,1},
        {1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1},
        {1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
        {1,0,1,0,1,0,1,0,1,1,2,1,1,0,1,0,1,0,1,0,1},
        {1,0,0,0,1,0,1,0,1,2,2,2,1,0,1,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1},
        {0,0,0,0,1,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0},
        {1,1,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1},
        {1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1},
        {1,0,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,0,1,0,1},
        {1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1},
        {1,1,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,1,1},
        {1,1,0,0,1,1,0,0,0,1,1,1,0,0,0,1,1,0,0,1,1},
        {1,0,0,1,0,1,1,0,0,0,1,0,0,0,1,1,0,1,0,0,1},
        {1,0,1,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };





while (app.isOpen())
{




    touche(app);
    app.clear();
    dessinNiveau(matrice,app,DIMENSION,TAILLE_FENETRE);
    app.display();
}

return EXIT_SUCCESS;
}
