#include <SFML/Graphics.hpp>
#include "deplacementJoueur.hpp"
#include <time.h>
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

using namespace sf;

char touche(RenderWindow &app);

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille);
int main()
{
    srand(time(NULL));
    Event evenement;
    char deplacement='n',nextdeplacement;
    int score=0,tele=0;
    Font font;
    if(!font.loadFromFile("arial.ttf"))
        printf("pb de chargement de la police\n");
    Text textBonus;
    char bonus[100];
    sprintf(bonus,"T�l�porteurs : %i",tele);
    textBonus.setString(bonus);
    textBonus.setFont(font);
    textBonus.setStyle(1);
    textBonus.setCharacterSize(25);
    textBonus.setColor(Color :: Cyan);
    Text texteScore;
    char text[20];
    sprintf(text,"Score : %i ",score);
    texteScore.setString(text);
    texteScore.setFont(font);
    texteScore.setStyle(1);
    texteScore.setCharacterSize(30);
    texteScore.setPosition(338,760);
    texteScore.setColor(Color :: Red);

    Time TempsActu;
	Time TempsTest;
	Time TempsPrec;
	Clock clock2;
	float Test = 0;
	int iTemps = 0;
	char timer[5];
	Text texteTemps;
	texteTemps.setFont(font);
    texteTemps.setCharacterSize(30);
    texteTemps.setPosition(600, 0);
    texteTemps.setColor(Color::Red);


    Entite J1;
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "JEU");
    //-1:Obstacle cassable, 0:Chemin , 1:Obstacle fixe, 2:Zone d'apparition des fant�mes, 3:M�t�orite(pour le score), 4:T�l�porteur(bonus), 5:Cl� � r�cup�rer(pour obstacle)
     int matrice[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,3,0,0,1,6,1,1,3,0,0,1,3,0,0,1,1,0,0,3,1},
        {1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1},
        {1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,0,1},
        {1,0,1,0,1,0,1,0,1,2,2,2,1,0,1,0,1,0,1,0,1},
        {1,0,0,0,1,0,1,3,1,1,2,1,1,3,1,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1},
        {0,0,0,0,1,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0},
        {1,1,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1},
        {1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1},
        {1,3,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,0,1,3,1},
        {1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1},
        {1,1,0,0,0,0,0,1,1,1,3,1,1,1,0,0,0,0,0,1,1},
        {1,1,0,1,1,1,0,0,1,1,-1,1,1,0,0,1,1,1,0,1,1},
        {1,0,0,1,3,1,1,0,0,1,-1,1,0,0,1,1,3,1,0,0,1},
        {1,0,1,1,0,1,4,1,0,0,0,0,0,1,4,1,0,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,1,6,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };


    app.clear();
    //dessinNiveau(matrice,app,DIMENSION);
    J1.posMX=10;
    J1.posMY=12;
    J1.posX=J1.posMX*LARGEUR_FENETRE/DIMENSION;
    J1.posY=J1.posMY*LARGEUR_FENETRE/DIMENSION;
    Texture pmR,pmL,pmU,pmD;
    Texture wall,textureAsteroide,textureTrouNoir,textureCle,textureCassable,textureCasse,textureItemFantome;
    Sprite mur,asteroide,trouNoir,obstacleCasse,obstacleCassable,cle,itemFantome;
    if (!pmR.loadFromFile("pooq moon petit.png")|| !pmL.loadFromFile("pooq moon left 35x35.png") ||
        !pmU.loadFromFile("pooq moon up 35x35.png") ||!pmD.loadFromFile("pooq moon down 35x35.png") ||
         !wall.loadFromFile("GalaxiePetit.png") || !textureAsteroide.loadFromFile("asteroide.png") ||
         !textureTrouNoir.loadFromFile("item trou noir.png") || !textureCassable.loadFromFile("GalaxiePetitCasse.png") ||
        !textureCasse.loadFromFile("galaxie trou 35x35.png") || !textureCle.loadFromFile("Cle.png") ||
        !textureItemFantome.loadFromFile("pookmoon famtome.png"))
        {
            printf("probleme chargement des images!");
            return EXIT_FAILURE;
        }
    cle.setTexture(textureCle);
    itemFantome.setTexture(textureItemFantome);
    obstacleCassable.setTexture(textureCassable);
    obstacleCasse.setTexture(textureCasse);
    asteroide.setTexture(textureAsteroide);
    trouNoir.setTexture(textureTrouNoir);
    mur.setTexture(wall);
    J1.objet.setTexture(pmR);
    J1.objet.setPosition(J1.posX,J1.posY);
    Texture image1,image2,image3,image4;
    image1.loadFromFile("famtome soleil vert 35px.png");
    image2.loadFromFile("famtome soleil orang e35px.png");
    image3.loadFromFile("famtome soleil bleu 35px.png");
    image4.loadFromFile("famtome galaxie 35px.png");

    Texture textureFantome[4];
    textureFantome[0] = image1;
    textureFantome[1] = image2;
    textureFantome[2] = image3;
    textureFantome[3] = image4;

    Sprite fantomes[10];

   /* fantome.setPosition(x,y); //faire un for pour les sprites
    fantome2.setPosition(x,y);
    fantome3.setPosition(x,y);
    fantome4.setPosition(x,y);
    fantome.setTexture(image);
    fantome2.setTexture(image);
    fantome3.setTexture(image);
    fantome4.setTexture(image);*/
    char directionFantomes[10] = {'H','H','H','H','H','H','H','H','H','H'};
    int x=10,y=7;
    x=x*LARGEUR_FENETRE/DIMENSION;
    y=y*HAUTEUR_FENETRE/DIMENSION;
    int directionDispo = 0;
    int xm,ym; char direction = 'H';
    char directionTableau[10] = {'D','G','B','H'};
    int nbFantome = 0;

    app.draw(J1.objet);
    app.display();
    J1=ObstacleVerif(matrice,J1);
    J1=positionMatricielle(J1);

    declarationFantome(fantomes,textureFantome,matrice);
    //animationGameover(score);
    while(!(YouWin(J1,matrice)))
    {
        if(iTemps>0)
        {
            TempsActu = clock2.getElapsedTime();
            TempsTest = TempsActu - TempsPrec;
            Test = TempsTest.asSeconds();
            if (Test > 1)
            {
            iTemps--;
            TempsPrec = TempsActu;
            }
        }
        nbFantome = 4 + (score/2);
        sprintf(text,"Score : %i ",score);
        sprintf(bonus,"T�l�porteurs : %i",tele);
        texteScore.setString(text);
        textBonus.setString(bonus);
        dessinNiveau(matrice,app,DIMENSION,mur,asteroide,trouNoir,obstacleCassable,obstacleCasse, cle,itemFantome);
        deplacements(J1,app,matrice,deplacement,nextdeplacement,score,tele,iTemps);
       dessinFantomes(x,y,xm,ym,fantomes,directionDispo,directionTableau,directionFantomes,app,matrice,nbFantome);
       if(iTemps>0)
       {
        TempsActu = clock2.getElapsedTime();
        TempsTest = TempsActu - TempsPrec;
        Test = TempsTest.asSeconds();
        if (Test > 1)
        {
            iTemps--;
            TempsPrec = TempsActu;
        }
        sprintf(timer,"%i",iTemps);
        texteTemps.setString(timer);
        app.draw(texteTemps);
       }

        app.draw(texteScore);
        app.draw(textBonus);
           app.display();
         app.clear();
         if(GameOver(J1,matrice,fantomes,iTemps,nbFantome))
            {
             app.close();
             animationGameover(score);
            }
         sleep(milliseconds(4));

    }
    app.close();
    animationYouWin(score);

    return EXIT_SUCCESS;
}

void dessinS(int x, int y,Sprite pic,RenderWindow &fenetre,int taille)
{

    pic.setPosition(x,y);
    fenetre.draw(pic);
}

void dessinNiveau(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille,Sprite pic,Sprite asteroide,Sprite trouNoir,Sprite obstacleCassable,Sprite obstacleCasse,Sprite Cle,Sprite itemFantome)
{
    int i;
    int f;
    for(i=0; i<taille; i++)
    {
        for(f=0; f<taille; f++)
        {
            if(niveau[f][i] == 0)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color::Black,fenetre,taille);
            else if(niveau[f][i] == 1)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),pic,fenetre,taille);
            else if(niveau[f][i] == 3)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),asteroide,fenetre,taille);
            else if(niveau[f][i] == 4)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),trouNoir,fenetre,taille);
            else if(niveau[f][i] == 2)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color(100,100,0),fenetre,taille);
            else if(niveau[f][i] == 5)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Cle,fenetre,taille);
            else if(niveau[f][i] == 6)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),itemFantome,fenetre,taille);
            else if(niveau[f][i] == -1)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),obstacleCassable,fenetre,taille);
            else if(niveau[f][i] == 7)
                dessinS(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),obstacleCasse,fenetre,taille);

        }
    }
}
char touche(RenderWindow &app)
{
    char res='n';
    Event event;

    while (app.pollEvent(event))
    {
        // Close window : exit
        if (event.type == Event::Closed)
        {
            app.close();
            return 'c';
        }
        if (event.type == Event::KeyPressed)
            switch(event.key.code)
            {
            case 57:
                res='s';
                break;
            case 25:
                res ='h';
                break;

            case 73:
                res ='h';
                break;

            case 16:
                res ='g';
                break;

            case 71:
                res ='g';
                break;

            case 18:
                res ='b';
                break;

            case 74:
                res ='b';
                break;

            case 3:
                res ='d';
                break;

            case 72:
                res ='d';
                break;

            default:
                res='n';
                break;
            }
    }



    return res;
}
