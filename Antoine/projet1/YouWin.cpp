#include <SFML/Graphics.hpp>
#include "deplacementJoueur.hpp"
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

using namespace sf;

char touche(RenderWindow &app);

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille);
void dessinNiveauBlue(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille,int couleur);
void animationYouWin(int score)
{

    Event evenement;
    char deplacement='n',nextdeplacement;
    Font font;
    Font fontGameOver;
    if(!font.loadFromFile("arial.ttf") || !fontGameOver.loadFromFile("Dedecus-BoldItalic.ttf"))
        printf("pb de chargement de la police\n");
    Sprite fondEcran;
    Texture texture;
    if(!texture.loadFromFile("trounoir.png"))
    {
        printf("pb de chargement de l'image\n");
    }
    fondEcran.setTexture(texture);
    fondEcran.setPosition(-450,-200);
    Text texteScore;
    char text[20];
    sprintf(text,"Score : %i ",score);
    texteScore.setString(text);
    texteScore.setFont(font);
    texteScore.setStyle(1);
    texteScore.setCharacterSize(30);
    texteScore.setPosition(338,450);
    texteScore.setColor(Color :: White);
    Text next;
    next.setString("Continuer");
    next.setFont(font);
    next.setStyle(1);
    next.setCharacterSize(70);
    next.setPosition(250,670);
    next.setColor(Color :: Green);
    Text texteGameOver;
    texteGameOver.setString("YOU WIN");
    texteGameOver.setFont(fontGameOver);
    texteGameOver.setCharacterSize(150);
    texteGameOver.setPosition(170,480);
    texteGameOver.setColor(Color :: White);

    Time TempsActu;
    Time TempsTest;
    Time TempsPrec;
    Clock clock2;
    float Test = 0;
    int iTemps = 30;
    char timer[20];
    Text texteTemps;

    sprintf(text, "Temps restant : %i ", iTemps);
    texteTemps.setString(text);
    texteTemps.setFont(font);
    texteTemps.setCharacterSize(30);
    texteTemps.setPosition(5,-2);
    texteTemps.setColor(Color::Yellow);


    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "YOU WIN");
    int matrice[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,0,0,1,0,1,1,0,0,0,1,0,0,0,1,1,0,0,0,1},
        {1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1},
        {1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
        {1,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,1},
        {1,0,0,0,1,0,1,3,1,1,0,1,1,3,1,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1},
        {0,0,0,0,1,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0},
        {1,1,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1},
        {1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1},
        {1,0,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,0,1,0,1},
        {1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1},
        {1,1,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,1,1},
        {1,1,0,1,1,1,0,0,1,1,1,1,1,0,0,1,1,1,0,1,1},
        {1,0,0,1,0,1,1,0,0,1,1,1,0,0,1,1,0,1,0,0,1},
        {1,0,1,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };


    app.clear();




    int couleur = 0;
    while(iTemps>0)
    {
        TempsActu = clock2.getElapsedTime();
        TempsTest = TempsActu - TempsPrec;
        Test = TempsTest.asSeconds();
        if (Test > 1)
        {
            iTemps--;
            TempsPrec = TempsActu;
        }
        sprintf(timer, "Temps Restant : %i ", iTemps);
        texteTemps.setString(timer);
        app.draw(fondEcran);
        dessinNiveauBlue(matrice,app,DIMENSION,couleur);
        app.draw(texteGameOver);
        app.draw(texteScore);
        app.draw(texteTemps);
        app.draw(next);
        app.display();

        sleep(milliseconds(10));
        app.clear();
        if(couleur!=255)
            couleur +=1;

        while (app.pollEvent(evenement))
        {

            switch (evenement.type)
            {

            case Event::Closed:
                app.close();
                break;
            case Event::MouseMoved:
                //Mise en �vidence du bouton qui permet de continuer � jouer
                if ((evenement.mouseMove.x >= 242 && evenement.mouseMove.x <= 558) && (evenement.mouseMove.y >= 684 && evenement.mouseMove.y <= 744))
                {
                    next.setCharacterSize(80);
                    next.setStyle(1);
                    next.setPosition(230,665);
                }
                else
                {
                    next.setCharacterSize(70);
                    next.setStyle(0);
                    next.setPosition(250,670);
                }
                break;
            case Event::MouseButtonPressed:
                if (evenement.mouseButton.x >= 242  && evenement.mouseButton.x <= 558 && evenement.mouseButton.y >= 684 && evenement.mouseButton.y <= 744)
                {
                    app.close();//A MODIFIER
                }
                break;

            }
        }
    }

    app.close();


}

void dessinNiveauBlue(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille, int couleur)
{
    int i;
    int f;
    for(i=0; i<taille; i++)
    {
        for(f=0; f<taille; f++)
        {

            if(niveau[f][i] == 0)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color(0,0,couleur),fenetre,taille);
            else if(niveau[f][i] == 3)
                dessinC(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color::Red,fenetre,taille);

        }
    }
}

