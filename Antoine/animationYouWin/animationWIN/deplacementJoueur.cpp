#include <SFML/Graphics.hpp>
#include "deplacementJoueur.hpp"
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800
#define PAS 1

using namespace sf;



Entite positionMatricielle(Entite J1)
{
    J1.posMX=((J1.posX-30)*DIMENSION/LARGEUR_FENETRE)+1;
    J1.posMY=((J1.posY-30)*DIMENSION/HAUTEUR_FENETRE)+1;
    return J1;
}
Entite positionReelle(Entite J1)
{
    J1.posX=(J1.posMX*LARGEUR_FENETRE/DIMENSION);
    J1.posY=(J1.posMY*HAUTEUR_FENETRE/DIMENSION);
    return J1;
}
Entite ObstacleVerif(int matrice[DIMENSION][DIMENSION],Entite player)
{
    player.verifTop=matrice[player.posMY-1][player.posMX];
    player.verifBot=matrice[player.posMY+1][player.posMX];
    player.verifRight=matrice[player.posMY][player.posMX+1];
    player.verifLeft=matrice[player.posMY][player.posMX-1];
    return player;
}



void deplacements(Entite &J1,RenderWindow &app,int matrice[DIMENSION][DIMENSION],char &deplacement,char &nextdeplacement,int &score,int &tele)
{

    Texture pmR,pmL,pmU,pmD;
    if (!pmR.loadFromFile("pooq moon petit.png")|| !pmL.loadFromFile("pooq moon left 35x35.png") ||
        !pmU.loadFromFile("pooq moon up 35x35.png") ||!pmD.loadFromFile("pooq moon down 35x35.png"))
        {
            printf("probleme chargement des images!");
        }
    char saisi;
    J1=ObstacleVerif(matrice,J1);
    saisi=touche(app);
    if(saisi!='n' && saisi!='s')
    {   //SAUVEGARDE DE DIRECTION
        if ((saisi=='h' && (J1.verifTop==1 || matrice[J1.posMY-1][J1.posMX-1]==1 || matrice[J1.posMY-1][J1.posMX+1]==1)) ||
             (saisi=='b' && (J1.verifBot==1 || matrice[J1.posMY+1][J1.posMX-1]==1 || matrice[J1.posMY+1][J1.posMX+1]==1)) ||
            (saisi=='g' && (J1.verifLeft==1 || matrice[J1.posMY-1][J1.posMX-1]==1 || matrice[J1.posMY+1][J1.posMX-1]==1))||
             (saisi=='d' && (J1.verifRight==1 || matrice[J1.posMY-1][J1.posMX+1]==1 || matrice[J1.posMY+1][J1.posMX+1]==1)))
            nextdeplacement=saisi;
        else
            deplacement=saisi;

    }
    if (((nextdeplacement=='h' && (J1.verifTop==0 || J1.verifTop>=3 ))  && verifPositionX(J1))  || ((nextdeplacement=='b' && (J1.verifBot==0 || J1.verifBot>=3)) && verifPositionX(J1))  ||
            ((nextdeplacement=='g' && (J1.verifLeft==0 || J1.verifLeft>=3)) && verifPositionY(J1))  || ((nextdeplacement=='d' && (J1.verifRight==0 || J1.verifRight>=3)) && verifPositionY(J1))  )
    {
        deplacement=nextdeplacement;
        nextdeplacement='n';
  }

    // TELEPORTATION TUNNEL
    if(J1.posMY==10 && J1.posMX==0)
    {
        J1.posMX=19;
        J1=positionReelle(J1);
    }
    if(J1.posMY==10 && J1.posMX==20)
    {
        J1.posMX=1;
        J1=positionReelle(J1);
    }



    if(deplacement=='h' && (!(verifPositionY(J1)) || J1.verifTop==0 || J1.verifTop>=3 ) && verifPositionX(J1))
        {
            J1.posY=J1.posY-PAS;
            J1.objet.setTexture(pmU);}
    else if(deplacement=='b' && (!(verifPositionY(J1)) || J1.verifBot==0 || J1.verifBot>=3)  && verifPositionX(J1) )
        {J1.posY=J1.posY+PAS;
        J1.objet.setTexture(pmD);}
    else if(deplacement=='g' && (!(verifPositionX(J1)) || J1.verifLeft==0 || J1.verifLeft>=3)  && verifPositionY(J1) )
        {J1.posX=J1.posX-PAS;
        J1.objet.setTexture(pmL);}
    else if(deplacement=='d' && (!(verifPositionX(J1)) || J1.verifRight==0 || J1.verifRight>=3)  && verifPositionY(J1) )
        {J1.posX=J1.posX+PAS;
        J1.objet.setTexture(pmR);}
    else
        nextdeplacement=saisi;

    // PIECE A RECUPERER
   if(matrice[J1.posMY][J1.posMX]==3)
        {
            matrice[J1.posMY][J1.posMX]=0;
            score+=1;
        }

    //GESTION DE L'ITEM DE TELEPORTATION
    if(matrice[J1.posMY][J1.posMX]==4 )
        {
            matrice[J1.posMY][J1.posMX]=0;
            tele+=1;
        }
    int mouseY=((Mouse::getPosition(app).y)*DIMENSION/HAUTEUR_FENETRE);
    int mouseX=((Mouse::getPosition(app).x)*DIMENSION/LARGEUR_FENETRE);
    //printf("%i,%i\n",mouseY,mouseX);
    if(saisi=='s' && tele>0 && matrice[mouseY][mouseX]==0)
        {
            teleporteur(J1,matrice,app);
            tele-=1;
            deplacement='n';
        }
    if(matrice[J1.posMY][J1.posMX]==5)
        {
            matrice[J1.posMY][J1.posMX]=0;
            matrice[16][10]=0;
            matrice[17][10]=0;

        }


    J1=positionMatricielle(J1);
    J1.objet.setPosition(J1.posX,J1.posY);


    app.draw(J1.objet);




}
int verifPositionX(Entite J1) //FONCTION QUI RENVOIE 1 OU 0 SI LE SPRITE EST CADRE SELON LA VERTICALE
{
    int res;
    if(J1.posX<=0+J1.posMX*TAILLE_FENETRE/DIMENSION && J1.posX>=(J1.posMX*TAILLE_FENETRE/DIMENSION)-0)
        res=1;
    else
        res=0;
    return res;
}
int verifPositionY(Entite J1) //FONCTION QUI RENVOIE 1 OU 0 SI LE SPRITE EST CADRE SELON L'HORIZONTALE
{
    int res;
    if(J1.posY<=0+J1.posMY*TAILLE_FENETRE/DIMENSION && J1.posY>=(J1.posMY*TAILLE_FENETRE/DIMENSION)-0)
        res=1;
    else
        res=0;
    return res;
}
int GameOver(Entite J1,int matrice[DIMENSION][DIMENSION]) //FONCTION QUI DIT SI C'EST FINI OU PAS (0 OU 1)
{
    int i,j;
    int res=1;
    for(i=0;i<DIMENSION;i++)
        for(j=0;j<DIMENSION;j++)
        {
            if(matrice[i][j]==3)
                res=0;
        }
    return res;
}
void teleporteur(Entite &J1,int matrice[DIMENSION][DIMENSION],RenderWindow &app) //TELEPORTE LE SPRITE A LA POSITION DE LA SOURIS
{

    J1.posX=Mouse::getPosition(app).x;
    J1.posY=Mouse::getPosition(app).y;
    J1=positionMatricielle(J1);
    J1=positionReelle(J1);

}

