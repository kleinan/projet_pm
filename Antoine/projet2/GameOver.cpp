#include <SFML/Graphics.hpp>
#include "deplacementJoueur.hpp"
#define DIMENSION 21
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 800
#define TAILLE_FENETRE 800

using namespace sf;

char touche(RenderWindow &app);

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille);
void dessinNiveauNew(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille,int couleur);
void animationGameover(int score)
{

    char deplacement='n',nextdeplacement;
    Font font;
    Font fontGameOver;
    if(!font.loadFromFile("arial.ttf") || !fontGameOver.loadFromFile("Dedecus-BoldItalic.ttf"))
        printf("pb de chargement de la police\n");
    Sprite fondEcran;
    Texture texture;
    if(!texture.loadFromFile("trounoir.png"))
    {
        printf("pb de chargement de l'image\n");
    }
    fondEcran.setTexture(texture);
    fondEcran.setPosition(-450,-200);
    Text texteScore;
    char text[20];
    sprintf(text,"Score : %i ",score);
    texteScore.setString(text);
    texteScore.setFont(font);
    texteScore.setStyle(1);
    texteScore.setCharacterSize(30);
    texteScore.setPosition(338,450);
    texteScore.setColor(Color :: White);
    Text texteGameOver;
    texteGameOver.setString("GAME OVER");
    texteGameOver.setFont(fontGameOver);
    texteGameOver.setCharacterSize(150);
    texteGameOver.setPosition(50,480);
    texteGameOver.setColor(Color :: White);




    Entite J1;
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "GAME OVER");
     int matrice[DIMENSION][DIMENSION]=
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,0,0,1,0,1,1,0,0,0,1,0,0,0,1,1,0,0,0,1},
        {1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,1},
        {1,0,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1},
        {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
        {1,0,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,1},
        {1,0,0,0,1,0,1,3,1,1,0,1,1,3,1,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1},
        {0,0,0,0,1,0,1,1,0,0,0,0,0,1,1,0,1,0,0,0,0},
        {1,1,1,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,1,1,1},
        {1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1},
        {1,0,1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,0,1,0,1},
        {1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,1},
        {1,1,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,1,1},
        {1,1,0,1,1,1,0,0,1,1,1,1,1,0,0,1,1,1,0,1,1},
        {1,0,0,1,0,1,1,0,0,1,1,1,0,0,1,1,0,1,0,0,1},
        {1,0,1,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,1,0,1},
        {1,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };


    app.clear();




    int couleur = 0;
    while(couleur<254)
    {
    app.draw(fondEcran);
    dessinNiveauNew(matrice,app,DIMENSION,couleur);
    app.draw(texteScore);
    app.draw(texteGameOver);
    app.display();

    sleep(milliseconds(10));
    app.clear();

    couleur +=1;
     }

    system("pause");

}

void dessinR(int x, int y,Color color,RenderWindow &fenetre,int taille)
{
    RectangleShape r(Vector2f(TAILLE_FENETRE/taille,TAILLE_FENETRE/taille));
    r.setPosition(x,y);
    r.setFillColor(Color(color.r,color.g,color.b));
    fenetre.draw(r);
}
void dessinC(int x, int y,Color color,RenderWindow &fenetre,int taille)
{
    int rayon=(TAILLE_FENETRE/taille)/4;
   CircleShape c(rayon);
    c.setPosition(x+rayon,y+rayon);
    c.setFillColor(Color(color.r,color.g,color.b));
    fenetre.draw(c);
}
void dessinNiveauNew(int niveau[DIMENSION][DIMENSION],RenderWindow &fenetre,int taille, int couleur)
{
    int i;
    int f;
    for(i=0; i<taille; i++)
    {
        for(f=0; f<taille; f++)
        {

            if(niveau[f][i] == 0)
                dessinR(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color(couleur,0,0),fenetre,taille);
            else if(niveau[f][i] == 3)
                dessinC(i*(TAILLE_FENETRE/taille),f*(TAILLE_FENETRE/taille),Color::Red,fenetre,taille);

        }
    }
}
